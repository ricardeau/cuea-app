# cuea-app

## Docker setup

```bash
 docker build -t cuea .
 docker run -p 8080:8080 cuea
```

## Local setup

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```