import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'mdi', // 'md'
  theme: {
    primary: '#242d64',
    secondary: '#242d64'
  }
})
