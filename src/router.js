import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/home/Index.vue'
import Menu from '@/views/menu/Index.vue'
import Transport from '@/views/transport/Index.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/menu',
      name: 'menu',
      component: Menu
    },
    {
      path: '/transport',
      name: 'transport',
      component: Transport
    }
  ]
})
