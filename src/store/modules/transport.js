import { UPDATE_NEXTBUSES, UPDATE_STATION } from './mutation-types'
import TransportService from '@/services/TransportService'

const stations = require('../../assets/bus-stops.json')

export const namespaced = true

export const state = {
  stations: stations,
  station: null,
  nextBuses: []
}

export const mutations = {
  [UPDATE_STATION](state, stationId) {
    state.station = state.stations.find(s => s.Id == stationId)
  },
  [UPDATE_NEXTBUSES](state, nextBuses) {
    state.nextBuses = nextBuses
  }
}

export const actions = {
  setStation({ commit }, stationId) {
    return new Promise(resolve => {
      commit(UPDATE_STATION, stationId)
      resolve()
    })
  },
  fetchNextBuses({ commit }) {
    return TransportService.getNextBuses(state.station.Id)
      .then(nextBuses => {
        commit(UPDATE_NEXTBUSES, nextBuses)
      })
      .catch(error => {
        const notification = {
          type: 'error',
          message:
            'There was a problem while fetching passages: ' + error.message
        }
        console.log(notification)
      })
  }
}
