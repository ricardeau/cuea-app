// vue.config.js
module.exports = {
  // options...
  devServer: {
    // proxy: 'https://yelo.agglo-larochelle.fr/'
    proxy: {
      '/api': {
        target: 'https://yelo.agglo-larochelle.fr',
        pathRewrite: { '^/api': '' },
        changeOrigin: true,
        secure: false
      }
    }
  },
  pwa: {
    name: 'Expensee',
    themeColor: '#4DBA87',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',

    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'src/service-worker.js'
    }
  }
}
